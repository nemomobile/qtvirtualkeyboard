Summary: Qt5 - VirtualKeyboard component
Name:    qt5-qtvirtualkeyboard
Version: 5.12.7
Release: 1
Group:   Qt/Qt
License: LGPLv3 with exception or GPLv3
URL:     http://www.qt.io
Source0: %{name}-%{version}.tar.xz
Patch0:  0001-defaultStyle-Use-a-white-handwriting-trace-for-bette.patch


BuildRequires: qt5-qtcore-devel >= 5.12.7
BuildRequires: qt5-qtgui-devel >= 5.12.7
BuildRequires: qt5-qtwidgets-devel >= 5.12.7
BuildRequires: qt5-qtdeclarative-devel >= 5.12.7
BuildRequires: qt5-qtdeclarative-qtquick-devel >= 5.12.7
BuildRequires: qt5-qtdeclarative-devel-tools >= 5.12.7
BuildRequires: qt5-qtsvg-devel >= 5.12.7
BuildRequires: qt5-qmake >= 5.12.7

Requires: qt5-qtsvg >= 5.12.7
Requires: qt5-qtwidgets >= 5.12.7
Requires: qt5-qtdeclarative-import-folderlistmodel >= 5.12.7

%description
The Qt Virtual Keyboard project provides an input
framework and reference keyboard frontend for Qt 5.

%package devel
Summary: Development files for %{name}
Requires: %{name} = %{version}-%{release}
%description devel
%{summary}.

%prep
%setup -q -n %{name}-%{version}/upstream
%patch0 -p1

%build
# Force fwd-include headers creation
touch .git

%{qmake5} \
    CONFIG+=disable-desktop \
    CONFIG+=lang-all

make %{?_smp_mflags}

%install
make install INSTALL_ROOT=%{buildroot}
rm -rf %{buildroot}/usr/lib/qt5/examples/virtualkeyboard/virtualkeyboard.pro

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc LICENSE.GPL3
%{_libdir}/libQt5VirtualKeyboard.so.5*
%{_libdir}/qt5/plugins/platforminputcontexts/*.so
%{_libdir}/qt5/plugins/virtualkeyboard/*.so
%{_libdir}/qt5/qml/QtQuick/VirtualKeyboard

%files devel
%defattr(-,root,root,-)
%{_includedir}/qt5/QtVirtualKeyboard/
%{_libdir}/cmake/Qt5Gui/Qt5Gui_QVirtualKeyboardPlugin.cmake
%{_libdir}/cmake/Qt5VirtualKeyboard
%{_libdir}/libQt5VirtualKeyboard.la
%{_libdir}/libQt5VirtualKeyboard.prl
%{_libdir}/libQt5VirtualKeyboard.so
%{_libdir}/pkgconfig/Qt5VirtualKeyboard.pc
%{_datadir}/qt5/mkspecs/modules
